angular.module("staff")
    .controller('headerController', headerController)
    .controller('homeController', homeController)
    .controller('kontaktController', kontaktController)
    .controller('proizvodiController', proizvodiController)
    .controller('loginController', loginController)
    .controller('kupacProizvodiController', kupacProizvodiController)
    .controller('kupacProizvodController', kupacProizvodController)
    .controller('porudzbineController', porudzbineController)
    .controller('proizvodController', proizvodController)

    .run(function($rootScope, $http, $state, $location, $window) {
        $rootScope.url = "http://staff-api.patrol-group.com/";
        if (localStorage.getItem('tokenKupac') != null) {
            $rootScope.token = localStorage.getItem('tokenKupac');
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.token;
        }
        $rootScope.$on('$locationChangeSuccess', function() {
            if (localStorage.getItem('tokenKupac') != null) {
                $rootScope.token = localStorage.getItem('tokenKupac');
                $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.token;
            }
        })
    });

function headerController($scope, $state) {

    // Header animation on scroll
    $(document).on('scroll', function() {
        if ($(window).scrollTop() > 50) {
            $('.navigation').css('background', 'white');
            TweenMax.to($('.navigation ul li'), .4, { color: "#495c66" });
            $('.navigation .logoDiv img').attr('src', 'img/logo/logo2.png');
            $('.navigation').css('box-shadow', '0 0 10px #e2e2e2');
            $('.navigation .links ul li').addClass('whiteAfter');
            $(".burger1, .burger2, .burger3").css('background', '#0E74B7');
        } else {
            $('.navigation').css('background', 'transparent');
            TweenMax.to($('.navigation ul li'), .4, { color: "white" });
            $('.navigation .logoDiv img').attr('src', 'img/logo/whiteLogo.png');
            $('.navigation .links ul li').removeClass('whiteAfter');
            $('.navigation').css('box-shadow', '0 0 0');
            $(".burger1, .burger2, .burger3").css('background', 'white');
        }
    })
    //////////////////////

    $scope.logout = function() {
        localStorage.setItem('tokenKupac', '');
        $state.go('login');
    }

    $('.subLink').on('mouseover', function() {
        TweenMax.set($('.submenu'), { display: "block" });
    }).on('mouseleave', function() {
        TweenMax.set($('.submenu'), { display: "none" });
    });

    var active = true;
    $scope.burger = function() {
        if (active) {
            TweenMax.to($('.burger2'), .4, { alpha: 0 });
            TweenMax.to($('.burger1'), .4, { rotation: 45, top: "10px" });
            TweenMax.to($('.burger3'), .4, { rotation: -45, top: "10px" });
            TweenMax.to($('.mobileMenu'), .4, { left: 0 });
            active = false;
        } else {
            TweenMax.to($('.burger2'), .4, { alpha: 1 });
            TweenMax.to($('.burger1'), .4, { rotation: 0, top: "0" });
            TweenMax.to($('.burger3'), .4, { rotation: 0, top: "16px" });
            TweenMax.to($('.mobileMenu'), .4, { left: "100%" });
            active = true;
        }
    }
}

function homeController() {
    $('.ukratkoDiv button').on('mouseover', function() {
        $('.ukratkoDiv button').addClass('gradient');
    }).on('mouseout', function() {
        $('.ukratkoDiv button').removeClass('gradient');
    })
}

function kontaktController($scope, $http) {
    var myLatLng = { lat: 43.313224, lng: 21.793204 };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: myLatLng,
        styles: [{
            "featureType": "all",
            "elementType": "geometry.fill",
            "stylers": [{ "weight": "2.00" }]
        }, {
            "featureType": "all",
            "elementType": "geometry.stroke",
            "stylers": [{ "color": "#9c9c9c" }]
        }, {
            "featureType": "all",
            "elementType": "labels.text",
            "stylers": [{ "visibility": "on" }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{ "color": "#f2f2f2" }]
        }, {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [{ "color": "#ffffff" }]
        }, {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [{ "color": "#ffffff" }]
        }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{ "saturation": -100 }, { "lightness": 45 }]
        }, {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [{ "color": "#eeeeee" }]
        }, {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [{ "color": "#7b7b7b" }]
        }, {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [{ "color": "#ffffff" }]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{ "visibility": "simplified" }]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{ "visibility": "off" }]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{ "visibility": "off" }]
        }, {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{ "color": "#46bcec" }, { "visibility": "on" }]
        }, {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [{ "color": "#c8d7d4" }]
        }, {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [{ "color": "#070707" }]
        }, { "featureType": "water", "elementType": "labels.text.stroke", "stylers": [{ "color": "#ffffff" }] }]
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
    });

    $scope.msg = {
        ime: "",
        email: "",
        poruka: ""
    };

    $scope.sendMsg = function(valid) {
        if (valid) {
            $http({
                method: "POST",
                url: "email/email.php",
                data: $scope.msg
            }).then(function() {
                $scope.success = "Poruka je uspešno poslata."
            })
        }
    }
}

function proizvodiController($scope, $http, $rootScope) {
    // setTimeout(function () {
    //     $(".owl-carousel").owlCarousel({
    //         nav: false,
    //         rewind: false,
    //         autoplay: false,
    //         dots: false,
    //         autoplayTimeout: 5000,
    //         responsive: {
    //             0: {
    //                 items: 1
    //             },
    //             600: {
    //                 items: 1
    //             },
    //             1000: {
    //                 items: 1
    //             }
    //         }
    //     });
    // }, 500);

    $scope.allProducts = function() {
        $scope.search = ""
        $http({
            method: "POST",
            url: $rootScope.url + "api/items-filtered/0",
            data: $scope.search
        }).then(function(res) {
            $scope.items = res.data.items;
            console.log(res.data);
            if (res.data.items.length == 0) {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
        })
    }

    $scope.closePopup = function() {
        $('.orderPopup').css('display', 'none');
        $('body').css('overflow-y', 'scroll');
    };

    $scope.getCats = function() {
        $http({
            method: "GET",
            url: $rootScope.url + "api/category"
        }).then(function(res) {
            $scope.cats = res.data.categories;
        })
    };
    $scope.getCats()

    $scope.changeCat = function(catId) {
        $http({
            method: "POST",
            url: $rootScope.url + "api/items-filtered/" + catId,
            data: $scope.search
        }).then(function(res) {
            $scope.items = res.data.items;
            if (res.data.items.length == 0) {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
        })
    }

    $scope.getItems = function() {
        $http({
            method: "GET",
            url: $rootScope.url + "api/items"
        }).then(function(res) {
            $scope.items = res.data;
            if (res.data.length == 0) {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
        })
    };
    $scope.getItems();

}

function proizvodController($scope, $http, $rootScope, $stateParams) {

    $http({
        method: "GET",
        url: $rootScope.url + "api/items/" + $stateParams.id
    }).then(function(res) {
        $scope.product = res.data;
    })

}

function loginController($scope, $http, $state, $rootScope) {
    $scope.auth = {
        email: "",
        password: ""
    };

    $scope.login = function() {
        $http({
            method: "POST",
            url: $rootScope.url + "api/users/signin",
            data: $scope.auth
        }).then(function(res) {
            localStorage.setItem('tokenKupac', res.data.token);
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
            setTimeout(function() {
                $state.go('kupac-proizvodi');
            }, 500)
        }).catch(function(res) {
            $scope.error = res.data.error;
        })
    }
}

function kupacProizvodiController($scope, $http, $rootScope) {
    // setTimeout(function () {
    //     $(".owl-carousel").owlCarousel({
    //         nav: false,
    //         rewind: false,
    //         autoplay: false,
    //         dots: false,
    //         autoplayTimeout: 5000,
    //         responsive: {
    //             0: {
    //                 items: 1
    //             },
    //             600: {
    //                 items: 1
    //             },
    //             1000: {
    //                 items: 1
    //             }
    //         }
    //     });
    // }, 500);

    $scope.weborder = {
        item_id: "",
        kolicina: 1,
        parts: [],
        boja_stepa: "",
        napomena_korisnika: ""
    }

    $scope.showPopup = function(proizvodId, nazivProizvoda) {
        $scope.weborder.item_id = proizvodId;
        $scope.nazivProizvoda = nazivProizvoda;
        $scope.hideBtn = false;
        $scope.message = "";
        $scope.weborder = {
            item_id: proizvodId,
            kolicina: 1,
            parts: [],
            boja_stepa: "",
            napomena_korisnika: ""
        }
        $('.orderPopup').css('display', 'block');
        $('body').css('overflow-y', 'hidden');
        $http({
            method: "GET",
            url: $rootScope.url + "api/items/" + proizvodId
        }).then(function(res) {
            $scope.proizvod = res.data;
            for (var i = 0; i < res.data.parts.length; i++) {
                $scope.weborder.parts.push({ part_id: res.data.parts[i].id, material: "" });
            }
            setTimeout(function() {
                console.log($scope.weborder);
            }, 500)
        });

        $http({
            method: "GET",
            url: $rootScope.url + "api/materials"
        }).then(function(res) {
            $scope.materials = {
                all: res.data
            }
        })
    };

    $scope.allProducts = function() {
        $scope.search = "";
        $http({
            method: "POST",
            url: $rootScope.url + "api/items-filtered/0",
            data: $scope.search
        }).then(function(res) {
            $scope.items = res.data.items;
            console.log(res.data);
            if (res.data.items.length == 0) {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
        })
    }

    $scope.naruci = function(valid) {
        if (valid) {
            $scope.hideBtn = true;
            $http({
                method: "POST",
                url: $rootScope.url + "api/weborders",
                data: $scope.weborder
            }).then(function() {
                $scope.message = "Porudžbina je uspešno završena."
            })
        }
    }

    $scope.closePopup = function() {
        $('.orderPopup').css('display', 'none');
        $('body').css('overflow-y', 'scroll');
    };

    $scope.getCats = function() {
        $http({
            method: "GET",
            url: $rootScope.url + "api/category"
        }).then(function(res) {
            $scope.cats = res.data.categories;
        })
    };
    $scope.getCats()


    $scope.changeCat = function(catId) {
        $http({
            method: "POST",
            url: $rootScope.url + "api/items-filtered/" + catId,
            data: $scope.search
        }).then(function(res) {
            $scope.items = res.data.items;
            if (res.data.items.length == 0) {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
        })
    }

    $scope.getItems = function() {
        $http({
            method: "GET",
            url: $rootScope.url + "api/items"
        }).then(function(res) {
            $scope.items = res.data;
            if (res.data.length == 0) {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
        })
    };
    $scope.getItems();
}

function kupacProizvodController($scope, $stateParams, $http, $rootScope) {
    $scope.closePopup = function() {
        $('.orderPopup').css('display', 'none');
        $('body').css('overflow-y', 'scroll');
    }

    $http({
        method: "GET",
        url: $rootScope.url + "api/items/" + $stateParams.id
    }).then(function(res) {
        $scope.product = res.data;
    })

    $scope.weborder = {
        item_id: $stateParams.id,
        kolicina: 1,
        parts: [],
        boja_stepa: "",
        napomena_korisnika: ""
    }

    $scope.showPopup = function(proizvodId, nazivProizvoda) {
        $scope.nazivProizvoda = nazivProizvoda;
        $scope.weborder = {
            item_id: proizvodId,
            kolicina: 1,
            parts: [],
            boja_stepa: "",
            napomena_korisnika: ""
        }
        $scope.hideBtn = false;
        $('.orderPopup').css('display', 'block');
        $('body').css('overflow-y', 'hidden');

        $http({
            method: "GET",
            url: $rootScope.url + "api/items/" + proizvodId
        }).then(function(res) {
            $scope.proizvod = res.data;

            for (var i = 0; i < res.data.parts.length; i++) {
                $scope.weborder.parts.push({ part_id: res.data.parts[i].id, material: "" });
            }

            setTimeout(function() {
                console.log($scope.weborder);
            }, 500)
        });

        $http({
            method: "GET",
            url: $rootScope.url + "api/materials"
        }).then(function(res) {
            $scope.materials = {
                all: res.data
            }
        })
    };

    $scope.naruci = function(valid) {
        if (valid) {
            $scope.hideBtn = true;

            $http({
                method: "POST",
                url: $rootScope.url + "api/weborders",
                data: $scope.weborder
            }).then(function() {
                $scope.message = "Porudžbina je uspešno završena."
            })
        }
    }
}

function porudzbineController($scope, $rootScope, $http) {
    $http({
        method: "GET",
        url: $rootScope.url + "api/webuser-orders"
    }).then(function(res) {
        $scope.weborders = res.data.weborders;

        for (var i = 0; i < res.data.weborders.length; i++) {
            console.log(res.data.weborders[i].parts)
            if (res.data.weborders[i].parts.length == 0) {
                $scope.weborders[i].emptyParts = true;
            } else {
                $scope.weborders[i].emptyParts = false;
            }
            $scope.weborders[i].created_at = new Date($scope.weborders[i].created_at);
        }
    })
}
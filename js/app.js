angular.module("staff", ['ui.router'])

    .config(function ($stateProvider, $locationProvider) {

        // $locationProvider.html5Mode({
        //     enabled: true,
        //     requireBase: true
        // });

        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: "view/home.html",
                controller: "homeController"
            })

            .state('about', {
                url: "/o-nama",
                templateUrl: "view/about.html"
            })

            .state('contact', {
                url: "/kontakt",
                templateUrl: "view/kontakt.html",
                controller: "kontaktController"
            })

            .state('products', {
                url: "/proizvodi",
                templateUrl: "view/proizvodi.html",
                controller: "proizvodiController"
            })

            .state('product', {
                url: "/proizvod/:id",
                templateUrl: "view/proizvod.html",
                controller: "proizvodController"
            })

            //Proizvodi
            .state('garniture', {
                url: "/garniture",
                templateUrl: "view/products/garniture.html"
            })
            .state('ugaone', {
                url: "/ugaone-garniture",
                templateUrl: "view/products/ugaone.html"
            })
            .state('lezajevi', {
                url: "/lezajevi",
                templateUrl: "view/products/lezajevi.html"
            })
            .state('francuski', {
                url: "/francuski",
                templateUrl: "view/products/francuski.html"
            })

            // Other part of site
            .state('login', {
                url: "/login",
                templateUrl: "view/hidden/login.html",
                controller: "loginController"
            })

            .state('kupac-proizvodi', {
                url: "/kupac/proizvodi",
                templateUrl: "view/hidden/proizvodi.html",
                controller: "kupacProizvodiController"
            })

            .state('kupac-proizvod', {
                url: "/kupac/proizvod/:id",
                templateUrl: "view/hidden/proizvod.html",
                controller: "kupacProizvodController"
            })

            .state('kupac-porudzbine', {
                url: "/kupac/porudzbine/",
                templateUrl: "view/hidden/porudzbine.html",
                controller: "porudzbineController"
            })

    })